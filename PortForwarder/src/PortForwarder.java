import java.io.IOException;

public class PortForwarder {
    public static final String WRONG_ARGUMENTS_NUMBER_ERROR = "Wrong arguments number";
    public static final String WRONG_PORT_ERROR = "Wrong port";

    public static void main(String[] args) { // <lport> <rhost> <rport>
        if (args.length != 3) {
            System.out.println(WRONG_ARGUMENTS_NUMBER_ERROR);
            return;
        }
        int lport, rport;
        try {
            lport = Integer.parseInt(args[0]);
            rport = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            System.out.println(WRONG_PORT_ERROR);
            return;
        }
        ForwarderSrv server = new ForwarderSrv(lport, args[1], rport);
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
