import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class ForwarderSrv {
    private static final int BUFFER_SIZE = 16834;

    private int lport;
    private String rhost;
    private int rport;

    private ServerSocketChannel serverSocket;
    private SelectionKey serverKey;
    private InetSocketAddress remoteAddress;
    private Map<SelectionKey, SelectionKey> connectedKeys;

    ForwarderSrv(int lport, String rhost, int rport) {
        this.lport = lport;
        this.rhost = rhost;
        this.rport = rport;
        serverSocket = null;
        connectedKeys = null;
    }

    public void start() throws IOException {
        Selector selector = Selector.open();
        serverSocket = ServerSocketChannel.open();
        serverSocket.bind(new InetSocketAddress("localhost", lport));
        serverSocket.configureBlocking(false);
        serverKey = serverSocket.register(selector, SelectionKey.OP_ACCEPT);
        connectedKeys = new LinkedHashMap<>();
        remoteAddress = new InetSocketAddress(rhost, rport);

        while (true) {
            selector.select();
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> iter = selectedKeys.iterator();
            while (iter.hasNext()) {
                SelectionKey key = iter.next();
                if (key == serverKey) {
                    openNewConnection(selector);
                } else forwardData(key);
                iter.remove();
            }
        }
    }

    private void openNewConnection(Selector selector) throws IOException {
        SocketChannel client = serverSocket.accept();
        client.configureBlocking(false);
        SelectionKey clientKey = client.register(selector, SelectionKey.OP_READ);

        SocketChannel remote = SocketChannel.open(remoteAddress);
        remote.configureBlocking(false);
        SelectionKey remoteKey = remote.register(selector, SelectionKey.OP_READ);

        connectedKeys.put(clientKey, remoteKey);
        connectedKeys.put(remoteKey, clientKey);
    }

    private void forwardData(SelectionKey key) throws IOException {
        SelectionKey keyTo = connectedKeys.get(key);
        ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
        SocketChannel fromChannel = (SocketChannel)key.channel();
        SocketChannel toChannel = (SocketChannel)keyTo.channel();
        fromChannel.read(buffer);
        buffer.flip();
        toChannel.write(buffer);
        buffer.clear();
    }
}
